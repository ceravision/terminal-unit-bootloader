
	radix DEC
    LIST    P=16F1827, F=INHX32         ; PIC Device-type & output HEX-file format 

xtal     EQU     4000000                
baud     EQU     9600                   

    #include "p16F1827.inc"
IdTypePIC = 0x3E      
	
	#include "SpbrgSelect.inc"

	#define max_flash 0x1000
	#define first_address    max_flash-132 ; BootLoaderSize # of words in size

	__CONFIG	_CONFIG1, _FOSC_INTOSC & _WDTE_ON & _PWRTE_OFF & _MCLRE_ON & _CP_OFF & _CPD_OFF & _BOREN_OFF & _CLKOUTEN_OFF & _IESO_ON & _FCMEN_ON
	__CONFIG    _CONFIG2, _WRT_OFF & _PLLEN_ON & _STVREN_ON & _BORV_19 & _LVP_OFF

	errorlevel 1, -305		

    cblock 0x020                       
    buffer:80
    endc

    cblock 0x70
    Crc
    BytesInBlock
    BlockCount
    ByteCount
    cnt1
    cnt2
    cnt3
    endc

   
SendByte  macro     TxChar
    movlw   TxChar
	movwf	TXREG
    endm

;0000000000000000000000000 RESET 00000000000000000000000000
    ORG     0x0000
	PAGESEL IntrareBootloader
	GOTO    IntrareBootloader

	ORG first_address
	nop
	nop
	nop
	nop
	org first_address+4
IntrareBootloader
	BANKSEL ANSELA                      ; Switch ->Bank3
    clrf    ANSELB                      ; ANSELx registers default to 1 at startup, Need to be 0 for digital-I/O & UART use
    bsf     BAUDCON,BRG16               ; Set BRG16 bit for high-speed baud generator (if =0, need to adjust calc of spbrg_value)
    clrf    SPBRGH                      ; Clear high-baud register
    movlw   spbrg_value                 ; This value should be 25 when using 4Mhz internal clock (with BRG16=1/SPBRGH=0)
    movwf   SPBRGL

    movlw   b'10010000'					;RCSTA=0x90; SPEN-RX9-SREN-CREN-ADDEN-FERR-OERR-RX9D
    movwf   RCSTA
    movlw   b'00100000'  				;TXSTA=0x20; CSRC-TX9-TXEN-SYNC-SENDB-BRGH-TRMT-TX9D
    movwf   TXSTA

    BANKSEL OSCCON                      ; Switch ->Bank1
    movlw   b'01101010'     			;0x6A
    movwf   OSCCON
    movlw   B'00000010'                 ; RB1 RX INPUT
    movwf   TRISB 

	;BANKSEL PORTA                      ; Switch ->Bank0
 	;bcf     PORTB,RB3
	;bsf		PORTB,RB4	
	;BANKSEL OSCCON                      ; Switch ->Bank1

    call    Receive                     ; Wait for computer to connect (return on Bank3)
    sublw   0xFB                        ; Expect 0xFB (start of flash-program upload)
    BNZ     Exit_Bootload               ; Branch to exit if 0xFA not rcvd
    SendByte IdTypePIC                  ; Send PIC type (to define flash-memory limits)

MainLoop                               ; back to original program
    SendByte 'K'                        ; Send 'K' to indicate ready for next block of data (return on Bank0)

mainl
    clrf    Crc
    call    Receive                     ; Expect High-byte of address (return on Bank3)
    movwf   EEADRH
    call    Receive                     ; Expect Low-byte of address (return on Bank3)
    movwf   EEADRL

    call    Receive                     ; Expect #chars in record (return on Bank3)
    movwf   BytesInBlock
    movwf   ByteCount
    incf    ByteCount,F                 ; Increment to receive last/CRC byte
    clrf    FSR0H                       ; Make sure we target the correct page/bank (might be able to remove this line)
    movlw   LOW buffer-1
    movwf   FSR0L   
	movlw   0x04                        ; Assume Block-count of 4 
    movwf   BlockCount
	movlw	0x10						;Load 16 BytesInBlock (8 words)
	movwf	BytesInBlock

RcvPgmBlock
    call    Receive                     ; Expect #chars in record (return on Bank3)
    incf    FSR0L,F
    movwf   INDF0
    decfsz  ByteCount,F
    goto    RcvPgmBlock

    movf    Crc,f                   ;Check if CRC=0
    BNZ     FailCrc
    movlw   LOW buffer
    movwf   FSR0L
    call    WriteData               ; Call WriteData on Bank3
    goto    MainLoop

WriteData                           ; Called from MainLoop (on Bank3)
	movlw   B'10111100'             ; Setup ERASING ROW (EEPGD=1/CFGS=0/LWLO=1/FREE=1/WRERR=1/WREN=1/WR=0/RD=0)
    movwf   EECON1
	call    RequiredSequence        ; Perform the required Word-Write sequence 
    bcf     EECON1,WREN             ; Disable writes to program-memory
	bcf 	EECON1,FREE 			; disable erase

WriteLoop                           ; Expected entry on Bank3 
   clrwdt
    movf    INDF0,W                 ; Load Pgm-data low-byte (2 bytes = 1 instruction)
    movwf   EEDATL
    incf    FSR0L,F
    movf    INDF0,W                 ; Load Pgm-data high-byte
    movwf   EEDATH
    incf    FSR0L,F

 	movlw   B'10101100'             ; Setup write to Program-memory (EEPGD=1/CFGS=0/LWLO=1/FREE=0/WRERR=1/WREN=1/WR=0/RD=0)
    movwf   EECON1
    call    RequiredSequence        ; Perform the required Word-Write sequence 
    incf    EEADRL,F                ; Assume still loading latches, increment Prog-memory address pointer
    decf    BytesInBlock,F
    decfsz  BytesInBlock,F
    goto    WriteLoop

	movlw	0x10						;reLoad 16 BytesInBlock (8 words)
	movwf	BytesInBlock

    call    WriteMemBlock
    decfsz  BlockCount,F
    goto    WriteLoop
    return


WriteMemBlock:
    decf    EEADRL,F                    ; Decrement Prog-memory address pointer (must be last addr in program block)
    bcf     EECON1,LWLO                 ; No more Latches only; Actually start write of program block
    call    RequiredSequence            ; Perform the required Word-Write sequence 
    bcf     EECON1,WREN                 ; Disable writes
	incf    EEADRL,F
	return



RequiredSequence
    movlw   0x55			; Start of required write sequence (must be in Bank3)
    movwf   EECON2
    movlw   0xaa
    movwf   EECON2
    bsf     EECON1,WR       ; Set WR bit to begin write
    NOP                     ; Any instructions here are ignored as processor halts to begin write sequence 
    NOP                     ; processor will stop here and wait for write complete after write processor continues with 3rd instruction
    return


FailCrc
    SendByte 'N'
    goto    mainl


Receive                  		;PIC16F1827 has RCSTA & RCREG on Bank3
    btfss   RCSTA,OERR    		; Clear any overrun error if found (ignored & not dealt with)
    goto    Rcv_NoOverrun
    bcf     RCSTA,CREN          ; Make sure any overrun error is cleared
    bsf     RCSTA,CREN
Rcv_NoOverrun
    movlw   xtal/2000000+1		
    movwf   cnt1
rpt2
    clrf    cnt2
rpt3
    clrf    cnt3
rptc
    BANKSEL PIR1                        ; Switch ->Bank0
    btfss   PIR1,RCIF                   ; Check for Rcvd-byte   
    goto    $+5
    BANKSEL RCREG                       ; Switch ->Bank3
    movf    RCREG,W                     ; Save byte in Wreg
    addwf   Crc,f                       ; CRC-calculation
    return

    clrwdt
    decfsz  cnt3,F
    goto    rptc
    decfsz  cnt2,F
    goto    rpt3
    decfsz  cnt1,F
    goto    rpt2

Exit_Bootload
    BANKSEL RCSTA                       ; Switch ->Bank3 (RCSTA in Bank3, not Bank0)
    bcf     RCSTA,SPEN                  ; Deactivate UART
	BANKSEL PORTA                       ; Switch ->Bank0

    goto    first_address

    END

